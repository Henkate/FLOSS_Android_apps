<!--
    Copyright (C)  2016 PRIMOKORN.
    Permission is granted to copy, distribute and/or modify this document
    under the terms of the GNU Free Documentation License, Version 1.3
    or any later version published by the Free Software Foundation;
    with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
    A copy of the license is included in the section entitled "GNU
    Free Documentation License".
-->
## NOTES / REMINDERS / TODO LIST

* [Laverna](https://laverna.cc/index.html): Laverna is a JavaScript note taking application with Markdown editor and encryption support. Consider it like open source alternative to Evernote.
![MPL2](https://img.shields.io/badge/License-MPL2-yellow.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/Laverna/laverna/)  
[![](Pictures/3rd-party.png)](https://github.com/Laverna/laverna/releases)

***

* [Memento](https://github.com/yaa110/Memento/releases/): A simple note taking app (categories, drawing notes, backup/restore, Rich Text WYSIWYG Editor...).
![MIT](https://img.shields.io/badge/License-MIT-orange.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/yaa110/Memento)  
[![](Pictures/3rd-party.png)](https://github.com/yaa110/Memento/releases/)

***

* [Mirakel](http://v.ht/NrjA): A simple but powerful tool for managing your TODO-lists. You can sync your lists with your own server.
![GPLv3+](https://img.shields.io/badge/License-GPLv3+-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/MirakelX/mirakel-android)  
[![](Pictures/F-Droid.png)](http://v.ht/NrjA)

***

* [NoteBuddy](https://f-droid.org/repository/browse/?fdid=nl.yoerinijs.notebuddy): Store encrypted notes.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/YoeriNijs/NoteBuddy)  
[![](Pictures/F-Droid.png)](https://f-droid.org/repository/browse/?fdid=nl.yoerinijs.notebuddy)

***

* [Note Crypt Pro](https://f-droid.org/repository/browse/?fdid=com.notecryptpro): Keep your notes safe and secure.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/RyuzakiKK/NoteCrypt)  
[![](Pictures/F-Droid.png)](https://f-droid.org/repository/browse/?fdid=com.notecryptpro)

***

* [Notepad](http://v.ht/Y7ds): Simple note taking app that let's you draw on notes.
![GPLv3+](https://img.shields.io/badge/License-GPLv3+-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/szafir1100/drawable-notepad)  
[![](Pictures/F-Droid.png)](http://v.ht/Y7ds)

***

* [Notes](https://f-droid.org/repository/browse/?fdid=org.secuso.privacyfriendlynotes): Create and manage notes. It is also possible to can define categories and assign them to the notes..
![GPLv3+](https://img.shields.io/badge/License-GPLv3+-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/SecUSo/privacy-friendly-notes)  
[![](Pictures/F-Droid.png)](https://f-droid.org/repository/browse/?fdid=org.secuso.privacyfriendlynotes)

***

* [Omni Notes FOSS](https://f-droid.org/repository/browse/?fdfilter=omni&fdid=it.feio.android.omninotes.foss): FOSS version of Omni Notes app.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/federicoiosue/Omni-Notes)  
[![](Pictures/F-Droid.png)](https://f-droid.org/repository/browse/?fdfilter=omni&fdid=it.feio.android.omninotes.foss)

***

* [Omni Notes](http://v.ht/isNI): Note taking open-source application aimed to have both a simple interface but keeping smart behavior.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/federicoiosue/Omni-Notes)  
[![](Pictures/Google_Play.png)](http://v.ht/isNI)  
_Uses Google Analytics, play-services, personal repos_

***

* [OpenTasks](http://v.ht/EO3q): A simple task manager app, allowing you to categorise your todo list by urgency, state, timeframe etc. Tasks can be synchronised with a CalDAV server using DAVdroid.
![Apache2](https://img.shields.io/badge/License-Apache%202.0-yellowgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/dmfs/opentasks)  
[![](Pictures/F-Droid.png)](http://v.ht/EO3q)

***

* [Orgzly](https://f-droid.org/repository/browse/?fdfilter=Orgzly&fdid=com.orgzly): Orgzly stores notebooks in plain-text and keeps them where you choose to.
![GPLv3+](https://img.shields.io/badge/License-GPLv3+-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/orgzly/orgzly-android)  
[![](Pictures/F-Droid.png)](https://f-droid.org/repository/browse/?fdfilter=Orgzly&fdid=com.orgzly)

***

* [Recurrence](http://v.ht/8KxI): Get reminded about notifications.
![Apache2](https://img.shields.io/badge/License-Apache%202.0-yellowgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/jonasbleyl/Recurrence)  
[![](Pictures/F-Droid.png)](http://v.ht/8KxI)

***

* [SimpleNote](http://v.ht/cWvo): The simplest way to keep notes. Light, clean, and free.
![GPLv2](https://img.shields.io/badge/License-GPLv2-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](http://v.ht/HOMR)  
[![](Pictures/Google_Play.png)](http://v.ht/cWvo)  
_Uses play-services, crashlytics and a lot of third-party repositories_

***

* [Simpletask Cloudless](https://f-droid.org/repository/browse/?fdfilter=simpletask&fdid=nl.mpcjanssen.simpletask): A simple task list manager that strives to have just enough features to do GTD (the Getting Things Done methodology), but no more.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/mpcjanssen/simpletask-android)  
[![](Pictures/F-Droid.png)](https://f-droid.org/repository/browse/?fdfilter=simpletask&fdid=nl.mpcjanssen.simpletask)

***

* [SyncOrg](https://f-droid.org/repository/browse/?fdid=com.coste.syncorg): Note taking app that supports Emacs OrgMode.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/wizmer/syncorg)  
[![](Pictures/F-Droid.png)](https://f-droid.org/repository/browse/?fdid=com.coste.syncorg)

***

* [TagSpaces](https://www.tagspaces.org/): TagSpaces is an offline, open source, personal data manager. It helps you organize files on your Windows, Linux, Android or Mac.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/tagspaces/tagspaces)  
[![](Pictures/Google_Play.png)](https://play.google.com/store/apps/details?id=org.tagspaces.androidle)

***

* [Tasks](http://v.ht/u13o): Fork of Astrid Tasks & To-Do List.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/abaker/tasks)  
[![](Pictures/F-Droid.png)](http://v.ht/u13o)

***

* [To-Do List](http://v.ht/Q4Wr): Create and manage tasks and To-Do lists.
![Apache2](https://img.shields.io/badge/License-Apache%202.0-yellowgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/SecUSo/privacy-friendly-todo-list)  
[![](Pictures/F-Droid.png)](http://v.ht/Q4Wr)

***

* [Turtl](http://v.ht/JG11): Turtl lets you take notes, bookmark websites, and store documents for sensitive projects. From sharing passwords with your coworkers to tracking research on an article you’re writing, Turtl keeps it all safe from everyone but you and those you share with.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/turtl/mobile)  
[![](Pictures/Google_Play.png)](http://v.ht/JG11)