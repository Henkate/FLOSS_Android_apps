<!--
    Copyright (C)  2016 PRIMOKORN.
    Permission is granted to copy, distribute and/or modify this document
    under the terms of the GNU Free Documentation License, Version 1.3
    or any later version published by the Free Software Foundation;
    with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
    A copy of the license is included in the section entitled "GNU
    Free Documentation License".
-->
## PRODUCTIVITY

* [AN2Linux](https://f-droid.org/app/kiwi.root.an2linuxclient): Sync Android notifications encrypted to a Linux desktop with tcp or bluetooth.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/rootkiwi/an2linuxclient)  
[![](Pictures/F-Droid.png)](https://f-droid.org/app/kiwi.root.an2linuxclient)

***

* [CuprumPDF](https://f-droid.org/repository/browse/?fdid=org.ninthfloor.copperpdf): View PDF files via pdf.js embedded into a WebView.
![Apache2](https://img.shields.io/badge/License-Apache%202.0-yellowgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/paride/CopperPDF)  
[![](Pictures/F-Droid.png)](https://f-droid.org/repository/browse/?fdid=org.ninthfloor.copperpdf)

***

* [LabCoat](https://f-droid.org/app/com.commit451.gitlab): GitLab client.
![Apache2](https://img.shields.io/badge/License-Apache%202.0-yellowgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-GitLab-lightgrey.svg?style=flat-square)](https://gitlab.com/Commit451/LabCoat)  
[![](Pictures/F-Droid.png)](https://f-droid.org/app/com.commit451.gitlab)

***

* [LnkShortener](https://f-droid.org/repository/browse/?fdfilter=LnkShortener&fdid=de.hirtenstrasse.michael.lnkshortener): Shortens URL using the Polr API.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/michaelachmann/LnkShortener)  
[![](Pictures/F-Droid.png)](https://f-droid.org/repository/browse/?fdfilter=LnkShortener&fdid=de.hirtenstrasse.michael.lnkshortener)

***

* [MuPDF](http://v.ht/E7l7): Lightweight document viewer. MuPDF supports PDF 1.7 with transparency, encryption, hyperlinks, annotations, searching, form editing and more. It also reads OpenXPS and CBZ (comic book) documents.
![AGPLv3+](https://img.shields.io/badge/License-AGPLv3+-green.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Ghostscript-lightgrey.svg?style=flat-square)](http://git.ghostscript.com/?p=mupdf.git;a=summary)  
[![](Pictures/F-Droid.png)](http://v.ht/E7l7)  
_[Material Design version here](http://v.ht/P3mE) (Android 5.0+)_

***

* [PDF Creator](http://v.ht/8dlR): Create and edit PDF files.
![GPLv3+](https://img.shields.io/badge/License-GPLv3+-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/scoute-dich/PDFCreator)  
[![](Pictures/F-Droid.png)](http://v.ht/8dlR)

***

* [ScreenCam Screen Recorder](http://v.ht/cEQz): (no root) User friendly minimal screen recorder for all devices with lollipop 5.0 and Up. It is opensourced and is completely free and ad free..
![AGPLv3](https://img.shields.io/badge/License-AGPLv3-green.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/vijai1996/screenrecorder)  
[![](Pictures/F-Droid.png)](https://f-droid.org/repository/browse/?fdfilter=screencam&fdid=com.orpheusdroid.screenrecorder)

***

* [SlimFileManager](https://forum.xda-developers.com/android/apps-games/app-slimfilemanager-t3515110): Open source, lightweight file manager.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/gmillz/SlimFileManager/tree/main)  
[![](Pictures/3rd-party.png)](https://www.androidfilehost.com/?fid=385035244224404539)  
_Uses Play Services_

***

* [TextFairy](https://play.google.com/store/apps/details?id=com.renard.ocr): Convert your phone into a mobile text scanner (OCR).
![Apache2](https://img.shields.io/badge/License-Apache%202.0-yellowgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/renard314/textfairy)  
[![](Pictures/Google_Play.png)](https://play.google.com/store/apps/details?id=com.renard.ocr)  
_Non-free dependencies: Play Services_

***

* [Turbo Editor](http://v.ht/clQ9): Simple, yet powerful editor for text files.
![GPLv3+](https://img.shields.io/badge/License-GPLv3+-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/vmihalachi/turbo-editor)  
[![](Pictures/F-Droid.png)](http://v.ht/clQ9)

***

* [Yalp Store](https://f-droid.org/repository/browse/?fdfilter=yalp&fdid=com.github.yeriomin.yalpstore): Download apks from Google Play Store.
![GPLv2](https://img.shields.io/badge/License-GPLv2-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/yeriomin/YalpStore)  
[![](Pictures/F-Droid.png)](https://f-droid.org/repository/browse/?fdfilter=yalp&fdid=com.github.yeriomin.yalpstore)  
_Promotes a non-Free network service_