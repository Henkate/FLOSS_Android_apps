<!--
    Copyright (C)  2016 PRIMOKORN.
    Permission is granted to copy, distribute and/or modify this document
    under the terms of the GNU Free Documentation License, Version 1.3
    or any later version published by the Free Software Foundation;
    with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
    A copy of the license is included in the section entitled "GNU
    Free Documentation License".
-->
## VIDEOS / AUDIO PLAYERS & EDITORS

* [AntennaPod](http://v.ht/HUqX): Podcast manager and player that gives you instant access to millions of free and paid podcasts, from independent podcasters to large publishing houses.
![MIT](https://img.shields.io/badge/License-MIT-orange.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/antennapod/AntennaPod)  
[![](Pictures/F-Droid.png)](http://v.ht/HUqX)

***

* [Encore](https://play.google.com/store/apps/details?id=com.fastbootmobile.encore.app): An open source, extendable music player for Android.
![GPL](https://img.shields.io/badge/License-GPL-brightgreen.svg?style=flat-square)[More information](https://github.com/fastbootmobile/encore/blob/master/LICENSES.md)  
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/fastbootmobile/encore)  
[![](Pictures/Google_Play.png)](https://play.google.com/store/apps/details?id=com.fastbootmobile.encore.app)  
_Uses proprietary play-services and some binary jar files_

***

* [Material Player](http://v.ht/fX3P): Simple audiobook player.
![GPLv3+](https://img.shields.io/badge/License-GPLv3+-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/Ph1b/MaterialAudiobookPlayer)  
[![](Pictures/F-Droid.png)](http://v.ht/fX3P)

***

* [Music](https://forum.xda-developers.com/android/apps-games/app-music-t3506636): A music player based on Auro.
![GPLv2](https://img.shields.io/badge/License-GPLv2-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/DominionOS/packages_apps_Music)  
[![](Pictures/3rd-party.png)](https://github.com/DominionOS/packages_apps_Music/releases/)  
_non-free dependencies: Google's firebase_

***

* [Music Player](http://v.ht/csxi): A clean music player with a customizable widget.
![Apache2](https://img.shields.io/badge/License-Apache%202.0-yellowgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/SimpleMobileTools/Simple-Music-Player)  
[![](Pictures/F-Droid.png)](http://v.ht/csxi)

***

* [NewPipe](http://v.ht/Re8H): Lightweight YouTube frontend that's supposed to be used without the proprietary YouTube-API or any of Google's (proprietary) play-services. NewPipe only parses the YouTube website in order to gain the information it needs.
![GPLv3+](https://img.shields.io/badge/License-GPLv3+-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/TeamNewPipe/NewPipe)  
[![](Pictures/F-Droid.png)](http://v.ht/Re8H)  
_Non-Free Network Services_

***

* [Odyssey](http://v.ht/bEUl): The main focus of this project is to create an music player that is optimized for speed (even with large music libraries). On the other hand the players is designed with the Material Design Guidelines in mind and we try to follow them as close as possible.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/gateship-one/odyssey)  
[![](Pictures/F-Droid.png)](http://v.ht/bEUl)

***

* [Phonograph](https://play.google.com/store/apps/details?id=com.kabouzeid.gramophone): A material designed local music player for Android.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/kabouzeid/Phonograph)  
[![](Pictures/Google_Play.png)](https://play.google.com/store/apps/details?id=com.kabouzeid.gramophone)

***

* [PodListen](http://v.ht/tG3t): Podcast player with lightweight interface.
![GPLv3+](https://img.shields.io/badge/License-GPLv3+-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/einmalfel/PodListen)  
[![](Pictures/F-Droid.png)](http://v.ht/tG3t)

***

* [Shuttle](https://play.google.com/store/apps/details?id=another.music.player): A famous music player.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://www.github.com/timusus/Shuttle)  
[![](Pictures/Google_Play.png)](https://play.google.com/store/apps/details?id=another.music.player)

***

* [SkyTube](http://v.ht/oatg): A simple and feature-rich YouTube player.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/ram-on/SkyTube)  
[![](Pictures/F-Droid.png)](http://v.ht/oatg)  
_Non-Free Network Services_

***

* [SoundWaves](http://v.ht/ZiuP): Manage, fetch and listen to podcasts.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/bottiger/SoundWaves)  
[![](Pictures/F-Droid.png)](http://v.ht/ZiuP)

***

* [Timber](http://v.ht/I0TI): Material Design Music Player.
![GPLv3+](https://img.shields.io/badge/License-GPLv3+-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/naman14/Timber)  
[![](Pictures/F-Droid.png)](http://v.ht/I0TI)

***

* [Transistor](http://v.ht/XLDQ): Transistor is a bare bones app for listening to radio programs over the internet.
![MIT](https://img.shields.io/badge/License-MIT-orange.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/y20k/transistor)  
[![](Pictures/F-Droid.png)](http://v.ht/XLDQ)

***

* [VLC](http://v.ht/mj27): Video and audio player that supports a wide range of formats, for both local and remote playback.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-GitLab-lightgrey.svg?style=flat-square)](https://code.videolan.org/videolan/vlc-android)  
[![](Pictures/F-Droid.png)](http://v.ht/mj27)

***

* [WebTube](http://v.ht/Cxvm): Lightweight YouTube frontend that's supposed to be used without the proprietary YouTube-API or any of Google's (proprietary) play-services.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/martykan/webTube)  
[![](Pictures/F-Droid.png)](http://v.ht/Cxvm)  
_Non-Free Network Services_