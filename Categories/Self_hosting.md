<!--
    Copyright (C)  2016 PRIMOKORN.
    Permission is granted to copy, distribute and/or modify this document
    under the terms of the GNU Free Documentation License, Version 1.3
    or any later version published by the Free Software Foundation;
    with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
    A copy of the license is included in the section entitled "GNU
    Free Documentation License".
-->
## SELF-HOSTING

* [Cozy cloud](https://cozy.io/en/): Cozy is an app-based personal cloud you host at home.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/cozy/cozy-mobile)  
[![](Pictures/Google_Play.png)](https://play.google.com/store/apps/details?id=io.cozy.files_client)

***

* [Goblim](http://v.ht/j5fn): Let's you share your photos on the Lut.im server of your choice (working with Framapic). Use your own server to regain control of your privacy!
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-GitLab-lightgrey.svg?style=flat-square)](https://git.mob-dev.fr/Schoumi/Goblim)  
[![](Pictures/F-Droid.png)](http://v.ht/j5fn)

***

* [MyOwnNotes](https://f-droid.org/repository/browse/?fdfilter=MyOwnNotes&fdid=org.aykit.MyOwnNotes): Create, edit, delete and sync your notes with ownCloud.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/aykit/MyOwnNotes)  
[![](Pictures/F-Droid.png)](https://f-droid.org/repository/browse/?fdfilter=MyOwnNotes&fdid=org.aykit.MyOwnNotes)

***

* [Nextcloud](https://f-droid.org/repository/browse/?fdid=com.nextcloud.client): A safe home for all your data. Access & share your files, calendars, contacts, mail & more from any device, on your terms. This is the official Android app.
![GPLv2](https://img.shields.io/badge/License-GPLv2-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/nextcloud/android)  
[![](Pictures/F-Droid.png)](https://f-droid.org/repository/browse/?fdid=com.nextcloud.client)

***

* [Nextcloud beta](https://f-droid.org/repository/browse/?fdid=com.nextcloud.android.beta): A safe home for all your data. Access & share your files, calendars, contacts, mail & more from any device, on your terms.
![GPLv2](https://img.shields.io/badge/License-GPLv2-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/nextcloud/android)  
[![](Pictures/F-Droid.png)](https://f-droid.org/repository/browse/?fdid=com.nextcloud.android.beta)

***

* [Nextcloud SMS](https://f-droid.org/repository/browse/?fdid=fr.unix_experience.owncloud_sms): App to synchronize your SMS messages on a remote NextCloud instance and let you read your messages from it.
![AGPLv3](https://img.shields.io/badge/License-AGPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/nerzhul/ownCloud-SMS-App)  
[![](Pictures/F-Droid.png)](https://f-droid.org/repository/browse/?fdid=fr.unix_experience.owncloud_sms)

***

* [OwnCloud](http://v.ht/KDTH): A free software package you can install on a server to manage files, contacts, calendars, music, pictures and much more. This is the official Android app which enables you to view and upload files.
![GPLv2](https://img.shields.io/badge/License-GPLv2-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/owncloud/android)  
[![](Pictures/F-Droid.png)](http://v.ht/KDTH)  
_Other OwnCloud apps are available on F-Droid_

***

* [Syncthing](http://v.ht/ZwZX): File synchronization. Syncthing replaces proprietary sync and cloud services with something open, trustworthy and decentralized.
![MPL2](https://img.shields.io/badge/License-MPL2-yellow.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/syncthing/syncthing-android)  
[![](Pictures/F-Droid.png)](http://v.ht/ZwZX)

***

* [Wallabag](http://v.ht/OdRj): A self hosted read-it-later app which is made for you to comfortably read and archive your articles. The server software is MIT-licensed.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/wallabag/android-app)  
[![](Pictures/F-Droid.png)](http://v.ht/OdRj)