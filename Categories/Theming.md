<!--
    Copyright (C)  2016 PRIMOKORN.
    Permission is granted to copy, distribute and/or modify this document
    under the terms of the GNU Free Documentation License, Version 1.3
    or any later version published by the Free Software Foundation;
    with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
    A copy of the license is included in the section entitled "GNU
    Free Documentation License".
-->

* [Blue Minimal](http://v.ht/ayrf): A Material Design inspired theme for Lollipop aiming to provide a consistent and minimalistic look to your device.
![GPLv3+](https://img.shields.io/badge/License-GPLv3+-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/scoute-dich/Baumann_Theme)  
[![](Pictures/F-Droid.png)](http://v.ht/ayrf)

***

* [ChocoUI](http://v.ht/c8qu): Chocolate styled theme for Cyanogenmod. It's based on the former XDA colors.
![Apache2](https://img.shields.io/badge/License-Apache%202.0-yellowgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/enricocid/ChocoUI---XDA-Colors)  
[![](Pictures/F-Droid.png)](http://v.ht/c8qu)

***

* [Enhancement](http://v.ht/6gzu): The goal is to provide a stock UI with enhanced icons and colors for your device, keeping it light and clean - CM13 theme.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-GitLab-lightgrey.svg?style=flat-square)](https://gitlab.com/Primokorn/EnhancementTheme)  
[![](Pictures/F-Droid.png)](https://f-droid.org/app/com.primokorn.enhancement)

***

* [Greyscale](http://v.ht/oGVb): A very simple and minimal theme, based on Material guidelines. The color scheme is entirely composed by shades of Material Grey.
![CC-BY-4.0](https://img.shields.io/badge/License-CC BY 4.0-9327E8.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/gabrielelucci/greyscale-cmte)  
[![](Pictures/F-Droid.png)](http://v.ht/oGVb)

***

* [ICEcons](http://v.ht/CQ9f): Icon pack for Trebuchet, Kiss, Nova, Apex, Holo and Adw launchers, also includes 4k wallpapers.
![GPLv3,CC-BY-SA-4.0](https://img.shields.io/badge/License-GPLv3, CC BY SA 4.0-16AB36.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/1C3/ICEcons)  
[![](Pictures/F-Droid.png)](http://v.ht/CQ9f)

***

* [Material Glass](http://v.ht/JPiI): Dark theme for CMTE and Substratum - Android 5.x and 6.x.
![Apache2](https://img.shields.io/badge/License-Apache%202.0-yellowgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/asdfasdfvful/Material-Glass)  
[![](Pictures/Google_Play.png)](https://play.google.com/store/apps/details?id=com.pitchedapps.material.glass.free)

***

* [MaterialOS](https://f-droid.org/repository/browse/?fdid=com.materialos.cm.theme): CM12/CM12.1. It features Material Design and Material Inspired illustrations and iconography from MaterialOS.
![CC-BY-4.0](https://img.shields.io/badge/License-CC BY 4.0-9327E8.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/materialos/CM12-Theme)  
[![](Pictures/F-Droid.png)](https://f-droid.org/repository/browse/?fdid=com.materialos.cm.theme)

***

* [Mysplash photography & wallpaper](https://play.google.com/store/apps/details?id=com.wangdaye.mysplash): Unsplash Client.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/WangDaYeeeeee/Mysplash)  
[![](Pictures/Google_Play.png)](https://play.google.com/store/apps/details?id=com.wangdaye.mysplash)

***

* [Paper FOSS Theme](http://v.ht/ZI8C): CM12 theme that is inspired by material design. It's aiming to provide a consistent and minimalistic look to your device.
![LGPLv3,CC-BY-SA-4.0](https://img.shields.io/badge/License-LGPLv3, CC BY SA 4.0-16AB36.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/beli3ver/Paper-FOSS-Theme)  
[![](Pictures/F-Droid.png)](http://v.ht/ZI8C)

***

* [Papuh](http://v.ht/77mm): Cloud based live wallpapers that are CC licensed.
![CC-BY-4.0](https://img.shields.io/badge/License-CC BY SA 4.0-9327E8.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/Papuh/PapuhWalls)  
[![](Pictures/F-Droid.png)](http://v.ht/77mm)