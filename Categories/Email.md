<!--
    Copyright (C)  2016 PRIMOKORN.
    Permission is granted to copy, distribute and/or modify this document
    under the terms of the GNU Free Documentation License, Version 1.3
    or any later version published by the Free Software Foundation;
    with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
    A copy of the license is included in the section entitled "GNU
    Free Documentation License".
-->
## EMAIL

* [Inbox Pager](http://v.ht/QOZN): E-mail client that supports IMAP, POP and SMTP protocols using SSL/TLS.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/itprojects/InboxPager)  
[![](Pictures/F-Droid.png)](http://v.ht/QOZN)

***

* [K-9 Mail](http://v.ht/scfZ): Full-featured email client supporting multiple accounts, POP3, IMAP and Push IMAP.
![Apache2](https://img.shields.io/badge/License-Apache%202.0-yellowgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/k9mail/k-9)  
[![](Pictures/F-Droid.png)](http://v.ht/scfZ)

***

* [K9-Mail Material Design](http://v.ht/h0Wj): Material Design version of K-9 mail.
![Apache2](https://img.shields.io/badge/License-Apache%202.0-yellowgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/scoute-dich/K9-MailClient)  
[![](Pictures/3rd-party.png)](http://forum.xda-developers.com/showpost.php?p=67674989&postcount=2)

***

* [Tutanota](https://tutanota.com/): Tutanota is the end-to-end encrypted email client that enables you to communicate securely with anyone.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/tutao/tutanota)  
[![](Pictures/F-Droid.png)](https://play.google.com/store/apps/details?id=de.tutao.tutanota)  
_Non-free dependencies: Play Services_