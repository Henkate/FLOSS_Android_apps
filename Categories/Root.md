<!--
    Copyright (C)  2016 PRIMOKORN.
    Permission is granted to copy, distribute and/or modify this document
    under the terms of the GNU Free Documentation License, Version 1.3
    or any later version published by the Free Software Foundation;
    with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
    A copy of the license is included in the section entitled "GNU
    Free Documentation License".
-->
## ROOT

* [iSU](https://forum.xda-developers.com/android/apps-games/isu-simple-app-to-deactivate-activate-t3478348): Deactivate/Activates CyanogenMod/LineageOS SU at will.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/bhb27/isu)  
[![](Pictures/3rd-party.png)](https://forum.xda-developers.com/android/apps-games/isu-simple-app-to-deactivate-activate-t3478348)

***

* [MagiskSU](http://v.ht/zoff): Magic Mask to Alter System Systemless-ly.  
[All source are mentioned there](http://forum.xda-developers.com/apps/magisk/official-magisk-v7-universal-systemless-t3473445)  
[![](Pictures/3rd-party.png)](http://v.ht/zoff)  
_Several files/threads to download/read_

***

* [phh's SuperUser](http://v.ht/UyAH): OpenSource SELinux-capable SuperUser. Fork of Koush's Superuser. Read the XDA thread for more information!
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/seSuperuser/Superuser)  
**Build bot**  
[![](Pictures/3rd-party.png)](https://superuser.phh.me/)  
**update.zip format**  
[![](Pictures/3rd-party.png)](https://superuser.phh.me/superuser.zip)

***

* [suhide-GUI](http://v.ht/kp3j): A simple GUI for adding and removing package UIDs using Chainfire's suhide.
![Apache2](https://img.shields.io/badge/License-Apache%202.0-yellowgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/loserskater/suhide-GUI)  
[![](Pictures/Google_Play.png)](https://play.google.com/store/apps/details?id=com.loserskater.suhidegui)