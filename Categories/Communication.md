<!--
    Copyright (C)  2016 PRIMOKORN.
    Permission is granted to copy, distribute and/or modify this document
    under the terms of the GNU Free Documentation License, Version 1.3
    or any later version published by the Free Software Foundation;
    with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
    A copy of the license is included in the section entitled "GNU
    Free Documentation License".
-->
## COMMUNICATION : IM / SMS / MMS / CHAT / TEAM COLLABORATION

* [BlitzMail / BlitzNote](https://f-droid.org/repository/browse/?fdfilter=blitzmail&fdid=de.grobox.blitzmail): To set up your email account once and then use it to send emails or short notes to an address of your choice.
![AGPLv3+](https://img.shields.io/badge/License-AGPLv3+-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/grote/BlitzMail)  
[![](Pictures/F-Droid.png)](https://f-droid.org/repository/browse/?fdfilter=blitzmail&fdid=de.grobox.blitzmail)

***

* [Conversations](http://v.ht/XAGO): XMPP client designed with ease of use and security in mind.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/siacs/Conversations)  
[![](Pictures/F-Droid.png)](http://v.ht/XAGO)

***

* [Delta Chat](https://f-droid.org/repository/browse/?fdid=com.b44t.messenger): Communicate instantly via e-mail.
![GPLv3+](https://img.shields.io/badge/License-GPLv3+-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/r10s/messenger-android)  
[![](Pictures/F-Droid.png)](https://f-droid.org/repository/browse/?fdid=com.b44t.messenger)

***

* [Kontalk](http://v.ht/uhWn): Secure instant messenger, allowing you to send and receive text, image and voice messages to and from other Kontalk users completely free of charge.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/kontalk/androidclient)  
[![](Pictures/F-Droid.png)](http://v.ht/uhWn)

***

* [I2P](https://f-droid.org/repository/browse/?fdid=net.i2p.android.router): I2P is an anonymizing network, offering a simple layer that identity-sensitive applications can use to securely communicate.
![Apache2](https://img.shields.io/badge/License-Apache%202.0-yellowgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/i2p)  
[![](Pictures/F-Droid.png)](https://f-droid.org/repository/browse/?fdid=net.i2p.android.router)

***

* [IRCCloud](https://play.google.com/store/apps/details?id=com.irccloud.android): Chat on IRC from anywhere, and never miss a message.
![Apache2](https://img.shields.io/badge/License-Apache%202.0-yellowgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/irccloud/android)  
[![](Pictures/Google_Play.png)](https://play.google.com/store/apps/details?id=com.irccloud.android)  
_Non-free dependencies: Play Services_

***

* [OpenKeychain](https://f-droid.org/repository/browse/?fdid=org.sufficientlysecure.keychain): Encrypt files and communications with OpenPGP.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/openpgp-keychain/openpgp-keychain)  
[![](Pictures/F-Droid.png)](https://f-droid.org/repository/browse/?fdid=org.sufficientlysecure.keychain)

***

* [QKSMS](http://v.ht/9laI): Replacement for the stock AOSP messaging app.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/qklabs/qksms)  
[![](Pictures/F-Droid.png)](http://v.ht/9laI)

***

* [Ring](https://f-droid.org/repository/browse/?fdid=cx.ring): Ring (formerly SFLphone) is a free distributed multimedia communication software. It is developed by Savoir-faire Linux with the help of a global community of users and contributors. Savoir-faire Linux is a Canadian company specialized in Linux and free software.
![GPLv3+](https://img.shields.io/badge/License-GPLv3+-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://ring.cx/en/documentation/faq#node-106)  
[![](Pictures/F-Droid.png)](https://f-droid.org/repository/browse/?fdid=cx.ring)

***

* [Riot](http://v.ht/klH6): Open team collaboration.
![Apache2](https://img.shields.io/badge/License-Apache%202.0-yellowgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/vector-im/vector-android)  
[![](Pictures/F-Droid.png)](http://v.ht/klH6)

***

* [Signal](http://v.ht/5XLr): Messaging app for simple private communication with friends.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/WhisperSystems/Signal-Android)  
[![](Pictures/Google_Play.png)](https://play.google.com/store/apps/details?id=org.thoughtcrime.securesms)  
_Non-free components. 3rd party builds NOT allowed. Google Play Services._

***

* [Silence](http://v.ht/3Do5): SMS/MMS application that allows you to protect your privacy while communicating with friends. Using Silence, you can send SMS messages and share media or attachments with complete privacy.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/SilenceIM/Silence)  
[![](Pictures/F-Droid.png)](http://v.ht/3Do5)

***

* [Simple IRC](https://f-droid.org/repository/browse/?fdid=tk.jordynsmediagroup.simpleirc.fdroid): A simple IRC client based on Atomic.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/jcjordyn130/simpleirc)  
[![](Pictures/F-Droid.png)](https://f-droid.org/repository/browse/?fdid=tk.jordynsmediagroup.simpleirc.fdroid)

***

* [Telegram](http://v.ht/4DyJ): Chat with friends, start group chats and share all kinds of content. All of your messages and conversations are stored in Telegram's cloud.
![GPLv2+](https://img.shields.io/badge/License-GPLv2+-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/slp/Telegram-FOSS)  
[![](Pictures/F-Droid.png)](http://v.ht/4DyJ)  
_Non-Free Network Services_